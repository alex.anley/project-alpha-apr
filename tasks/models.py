from django.db import models
from django.conf import settings
from projects.models import Project

# Create your models here.


task_relations = (
    ("none", "- none -"),
    ("startstart", "Start to Start"),
    ("endstart", "End to Start"),
    ("endend", "End to End"),
)


class Task(models.Model):
    name = models.CharField(max_length=200)
    notes = models.TextField(null=True, blank=True)
    start_date = models.DateTimeField(
        auto_now=False, auto_now_add=False, blank=True
    )
    due_date = models.DateTimeField(
        auto_now=False, auto_now_add=False, blank=True
    )
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )
    related_task = models.ForeignKey(
        "self",
        related_name="dependencies",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        default="self",
    )
    relationship = models.CharField(
        max_length=20, choices=task_relations, default="none"
    )
    task_duration = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.project} / {self.name}"
