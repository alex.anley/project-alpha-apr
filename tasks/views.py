from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from datetime import timedelta

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        valid = True
        task = form.save(False)
        print("this is the saved form object", task.start_date)
        # if task.start_date > task.due_date:
        #     form = TaskForm()
        # form.add_error("start_date","Invalid dates.")

        if form.is_valid():
            task = form.save(False)

            if task.relationship == "none":
                task.save()
            elif task.relationship == "startstart":
                task.start_date = task.related_task.start_date
                task.due_date = task.start_date + timedelta(
                    days=task.task_duration
                )
                task.save()
            elif task.relationship == "endstart":
                task.start_date = task.related_task.due_date
                task.due_date = task.start_date + timedelta(
                    days=task.task_duration
                )
                task.save()
            elif task.relationship == "endend":
                task.due_date = task.related_task.due_date
                task.start_date = task.due_date - timedelta(
                    days=task.task_duration
                )
                task.save()

            return redirect("list_projects")

        else:
            form = TaskForm()
            form.add_error("name", "invalid form values")

    else:
        form = TaskForm()
        context = {"form": form}

        return render(request, "projects/task_create.html", context)


@login_required
def show_tasks(request):
    show_tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": show_tasks}
    return render(request, "projects/mine.html", context)
