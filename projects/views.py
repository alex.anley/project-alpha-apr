from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.models import Project
from tasks.models import Task
from projects.forms import ProjectForm
import pandas
from plotly.offline import plot
import plotly.express as px


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    show_project = Project.objects.get(id=id)
    context = {
        "show_project": show_project,
    }
    return render(request, "projects/detail.html", context)

@login_required
def show_gantt(request, pk):

    project = get_object_or_404(Project, id=pk)
    tasks = Task.objects.filter(project=project.id).order_by('start_date')
    tasks_objs = []
    tasks_data = []

    def append_dependency(new_task_obj):
        if new_task_obj not in tasks_objs:
            tasks_objs.append(new_task_obj)
        for task in new_task_obj.dependencies.all():
            append_dependency(task)

    for x in tasks:
        append_dependency(x)
        print(tasks_objs)

    for x in tasks_objs:
        tasks_data.append({
            'Task': x.name,
            'Start': x.start_date,
            'Finish': x.due_date,
            'Completed': x.is_completed
            })

    print(tasks_data)

    df = pandas.DataFrame(tasks_data)
    fig = px.timeline(
        df, x_start="Start", x_end="Finish", y="Task", color="Completed", # template="plotly_dark",
    )
    fig.update_yaxes(autorange="reversed")
    gantt_plot = plot(fig, output_type="div")
    context = {
        'plot_div': gantt_plot,
        "tasks": tasks,
        "project": project,
    }
    return render(request, "projects/proj_gantt.html", context)

@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)

def homie(request):
    return render(request, "projects/homie.html", {})

def aboutus(request):
    return render(request, "projects/aboutus.html", {})
