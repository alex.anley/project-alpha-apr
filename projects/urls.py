from projects.views import list_projects, show_project, create_project, show_gantt, homie, aboutus
from django.urls import path, include


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("accounts/", include("accounts.urls")),
    path("<int:id>/", show_project, name="show_project"),
    path("<int:pk>/gantt/", show_gantt, name="show_gantt"),
    path("create/", create_project, name="create_project"),
    path("homie/", homie, name="homie"),
    path("aboutus/", aboutus, name="aboutus"),
]
