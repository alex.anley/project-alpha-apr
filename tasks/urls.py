from tasks.views import create_task, show_tasks
from django.urls import path


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_tasks, name="show_my_tasks"),
]
